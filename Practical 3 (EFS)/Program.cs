﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practical_3__EFS_
{
    class Program
    {
        internal class Point
        {
            public int X { get; set; } = 0;
            public int Y { get; set; } = 0;

            public Point(int x, int y)
            {
                this.X = x;
                this.Y = y;
            }

            public override string ToString()
            {
                return String.Format($"Point:({X};{Y})");
            }

            public override bool Equals(Object obj)
            {
                if (obj == null || GetType() != obj.GetType())
                    return false;

                Point p = (Point) obj;
                return (X == p.X) && (Y == p.Y);
            }

            public override int GetHashCode()
            {
                return X ^ Y;
            }
        }

        internal class Line
        {
            public Point StartPoint { get; set; }
            public Point EndPoint { get; set; }

            public Line(Point startPoint, Point endPoint)
            {
                StartPoint = startPoint;
                EndPoint = endPoint;
            }

            public double Lenght()
            {
                return Math.Sqrt(Math.Pow((EndPoint.Y - StartPoint.Y), 2) + Math.Pow((EndPoint.X - StartPoint.X), 2));
            }

        }

        public abstract class Tetragon
        {
            public Point[] Points { get; set; }

            public Tetragon(Point[] points)
            {
                Points = points;
            }

            private void DrawLine(int w, char ends, char mids)
            {
                Console.Write(ends);
                for (int i = 1; i < w - 1; ++i)
                    Console.Write(mids);
                Console.WriteLine(ends);
            }

            public void DrawBox()
            {
                int w = Convert.ToInt32(new Line(Points[1], Points[2]).Lenght());
                int h = Convert.ToInt32(new Line(Points[0], Points[1]).Lenght());
                DrawLine(w, '*', '*');
                for (int i = 1; i < h - 1; ++i)
                    DrawLine(w, '*', ' ');
                DrawLine(w, '*', '*');
            }


            public virtual bool IsFigure()
            {
                for (int i = 0; i < 3; i++)
                {
                    if (Points[i].Equals(Points[i + 1]))
                    {
                        return false;
                    }
                }

                if (Points[0].Equals(Points[3]) | (Points[1].X > Points[2].X & Points[0].X < Points[3].X))
                {
                    return false;
                }

                if (Intersection(Points[0], Points[1], Points[2]))
                {
                    if (Intersection(Points[1], Points[2], Points[3]))
                    {
                        if (Intersection(Points[2], Points[3], Points[0]))
                        {
                            return true;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }

            public bool Intersection(Point A, Point B, Point C)
            {
                int xo = A.X, yo = A.Y;
                int p = B.X - A.X, q = B.Y - A.Y;

                int x1 = B.X, y1 = B.Y;
                int p1 = C.X - B.X, q1 = C.Y - B.Y;

                int x = (xo * q * p1 - x1 * q1 * p - yo * p * p1 + y1 * p * p1) /
                        (q * p1 - q1 * p);
                int y = (yo * p * q1 - y1 * p1 * q - xo * q * q1 + x1 * q * q1) /
                        (p * q1 - p1 * q);

                if (B.Equals(new Point(x, y)))
                {
                    return true;
                }
                return false;
            }
        }

        public class Rectangle : Tetragon
        {
            public Rectangle(Point[] points) : base(points)
            {

            }

            public override bool IsFigure()
            {
                if (base.IsFigure())
                {
                    if (new Line(Points[1], Points[3]).Lenght().Equals(new Line(Points[0], Points[2]).Lenght()))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                return false;
            }

            public static Rectangle CreateInstance(Point [] points)
            {
                return new Rectangle(points).IsFigure() ? new Rectangle(points) : null;
            }
        }

        public class Square : Rectangle
        {
            public Square(Point[] points) : base(points)
            {

            }
        
            public override bool IsFigure()
            {
                if (base.IsFigure())
                {
                    if (new Line(Points[0], Points[1]).Lenght().Equals(new Line(Points[1], Points[2]).Lenght()))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }

            public new static Square CreateInstance(Point[] points)
            {
                return new Square(points).IsFigure() ? new Square(points) : null;
            }
        }

        static List<Tetragon> alfa = new List<Tetragon>();

        static void Main(string[] args)
        {
            Console.WriteLine("Enter:\n" +
                              "F1) Rectangle\n"
                              + "F2) Square\n"
                              + "F3) Show\n"
                              + "ESC) Exit");

            ConsoleKeyInfo key;

            do
            {
                key = Console.ReadKey();

                if (key.Key == ConsoleKey.F1)
                {
                    Point[] points = new Point[4];
                    Console.WriteLine("ENTER POINT:");

                    for (int i = 0; i < 4; i++)
                    {
                        Console.Write("Point[{0}]:", i);
                        int[] valueArray = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
                        points[i] = new Point(valueArray[0], valueArray[1]);
                    }

                    var objSquare = Rectangle.CreateInstance(points);

                    if (objSquare != null)
                    {
                        alfa.Add(objSquare);
                    }
                    else
                    {
                        Console.WriteLine("You enter not valid data for rectangle.");
                    }

                }
                else if (key.Key == ConsoleKey.F2)
                {
                    Point[] points = new Point[4];

                    Console.WriteLine("ENTER POINT:");

                    for (int i = 0; i < 4; i++)
                    {
                        Console.Write("Point[{0}]:", i);
                        int[] valueArray = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
                        points[i] = new Point(valueArray[0], valueArray[1]);
                    }

                    var objSquare= Square.CreateInstance(points);

                    if (objSquare != null)
                    {
                         alfa.Add(objSquare);
                    }
                    else
                    {
                        Console.WriteLine("You enter not valid data for square.");
                    }
                }
                else if (key.Key == ConsoleKey.F3)
                {
                    foreach (var obj in alfa)
                    {
                        Console.WriteLine("-------------");
                        obj.DrawBox();
                    }
                }
            } while (key.Key != ConsoleKey.Escape);

            Console.ReadKey();
        }
    }
}

